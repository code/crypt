`crypt(1)`
==========

A convenience wrapper for UNIX `crypt(3)`. Includes a manual page and `-h`
output and nothing else.

    $ make
    $ sudo make install

Or put it somewhere else:

    $ make install PREFIX="$HOME"/.local

License
-------

Copyright (C) 2015--2017, 2020--2021 Tom Ryder <tom@sanctum.geek.nz>

Distributed under GNU General Public License version 3 or any later version.
Please see `COPYING`.
