.POSIX:
.PHONY: all clean distclean install
PREFIX = /usr/local
#CC = gcc
#CFLAGS = -Wall -Werror -Wextra -ansi -pedantic
LDFLAGS = -lcrypt
all: crypt
install:
	mkdir -p -- $(PREFIX)/bin
	cp -- crypt $(PREFIX)/bin
	mkdir -p -- $(PREFIX)/share/man/man1
	cp -- crypt.1 $(PREFIX)/share/man/man1
clean distclean:
	rm -f -- crypt
